<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>ナンプレテスト</title>
	<link rel="stylesheet/less" href="./less/main.less">
	<script src="./lib/js/autoprefixer.js"></script>
	<script>
		less = {
			postProcessor:function(css){
				return autoprefixer.process(css).css;
			},
			logLevel:1

		}
	</script>
	<script src="./lib/js/less-1.7.4.min.js"></script>
	<script>
		less.watch();
	</script>
</head>
<body>
	<main>
		<table>
			<tr v-repeat="rowTable">
				<td v-repeat="$value" v-class="hint:visible" v-on="click:selectGrid" v-text="(val2===0)?'':val2"></td>
			</tr>
		</table>
		<div class="console">
			<table>
				<tr>
					<td v-on="click:clearAnswer" v-text="'C'"></td>
					<td v-repeat="choiceTable" v-on="click:selectAnswer" v-text="$value"></td>
				</tr>
			</table>
		</div>
	</main>
	<script src="./js/main.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="./lib/js/vue.min.js"></script>
	<script>
	var numple = new TCG.Numple;
	numple.generate();
	var cell;
	window.onload = function(){
		var vue = new Vue({
			el:'main',
			data:{
				rowTable:numple.hlines,
				choiceTable:[1,2,3,4,5,6,7,8,9]
			},
		    methods:{
				selectGrid:function(e){
					// ヒントマスは除く
					if(!e.targetVM.visible){
						var g = document.querySelector('.selected');
						if(g){
							g.classList.remove('selected');
							cell = undefined;
						}
						cell = e.targetVM;
						e.targetVM.$el.classList.add('selected');
					}
				},
				selectAnswer:function(e){
					var g = document.querySelector('.selected');
					if(g && g===cell.$el){
						cell.val2 = e.targetVM.$value;
					}
					if(numple.grids.every(function(g){return g.val2 !== 0})){
						// 全部揃ったら答え合わせ
						if(numple.grids.some(function(g){return g.val2 !== g.val})){
							alert("間違い！")
						} else {
							alert("正解！")
						}
					}

				},
				clearAnswer:function(e){
					var g = document.querySelector('.selected');
					if(g && g===cell.$el){
						cell.val2 = 0;
					}
				}
		    }
		});
	}
	</script>
	
</body>
</html>