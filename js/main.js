/// <reference path='../d.ts/DefinitelyTyped/jquery/jquery.d.ts' />
'use strict';
Array.prototype.shuffle = function () {
    var i = this.length;
    while (i) {
        var j = Math.floor(Math.random() * i);
        var t = this[--i];
        this[i] = this[j];
        this[j] = t;
    }
    return this;
};
var TCG;
(function (TCG) {
    function capitaliseFirstLetter(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    TCG.capitaliseFirstLetter = capitaliseFirstLetter;
    function toCamelCase(str) {
        return str.replace(/(\-([a-z]))/g, function ($1, $2, $3) {
            return $3.toUpperCase();
        });
    }
    TCG.toCamelCase = toCamelCase;
    var Numpre = (function () {
        function Numpre(arg) {
            this.grids = [];
            this.hlines = [];
            this.vlines = [];
            this.blocks = [];
            this.iter = 0;
            var o, b;
            for (var i = 1; i <= 9; i++) {
                for (var j = 1; j <= 9; j++) {
                    b = Math.floor((i - 1) / 3) * 3 + Math.floor((j - 1) / 3) + 1;
                    o = { x: j, y: i, block: b, cand: [], val: 0, val2: 0, visible: true };
                    this.grids.push(o);
                }
            }
            var x, y;
            for (i = 1; i <= 9; i++) {
                this.hlines[i] = this.grids.filter(function (g) {
                    return (g.y === i);
                });
                this.vlines[i] = this.grids.filter(function (g) {
                    return (g.x === i);
                });
                this.blocks[i] = this.grids.filter(function (g) {
                    return (g.block === i);
                });
            }
        }
        Numpre.prototype.init = function () {
            this.grids.forEach(function (g) {
                g.cand = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                g.val = 0;
            });

            // ブロック1,5,9にランダムで値を入れてしまう
            var candNum1 = [1, 2, 3, 4, 5, 6, 7, 8, 9].shuffle();
            var candNum2 = [1, 2, 3, 4, 5, 6, 7, 8, 9].shuffle();
            var candNum3 = [1, 2, 3, 4, 5, 6, 7, 8, 9].shuffle();
            for (var i = 0; i < 9; i++) {
                this.blocks[1][i].val = candNum1[i];
                this.blocks[5][i].val = candNum2[i];
                this.blocks[9][i].val = candNum3[i];
                this.blocks[1][i].cand = [];
                this.blocks[5][i].cand = [];
                this.blocks[9][i].cand = [];
            }
        };

        // 仮定法再帰は無い
        Numpre.prototype.solve = function () {
            var _this = this;
            var result;
            var emptyGrids = this.grids.filter(function (e) {
                return (e.val2 === 0);
            });
            emptyGrids.forEach(function (g) {
                var cand = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                var imax = cand.length;
                var ary;
                var index;
                var v;
                for (var i = imax - 1; i >= 0; i--) {
                    v = cand[i];
                    ary = [];
                    Array.prototype.push.apply(ary, _this.blocks[g.block]);
                    Array.prototype.push.apply(ary, _this.vlines[g.x]);
                    Array.prototype.push.apply(ary, _this.hlines[g.y]);
                    if (ary.some(function (e) {
                        return (e.val2 === v);
                    })) {
                        cand.splice(i, 1);
                    }
                }

                // 値が1つなら確定させる
                if (cand.length === 1) {
                    g.val2 = cand[0];

                    // 確定したら再帰
                    _this.solve();
                }
            });
            return this.grids.every(function (e) {
                return (e.val2 !== 0);
            });
        };
        Numpre.prototype.compensate = function () {
            var _this = this;
            this.iter++;
            this.minLength = 9;

            // 確定してない場合候補から縦横ブロックに存在する値を除く
            var emptyGrids = this.grids.filter(function (e) {
                return (e.val === 0);
            });
            emptyGrids.forEach(function (g) {
                var imax = g.cand.length;
                var ary;
                var v;
                for (var i = imax - 1; i >= 0; i--) {
                    v = g.cand[i];
                    ary = [];
                    Array.prototype.push.apply(ary, _this.blocks[g.block]);
                    Array.prototype.push.apply(ary, _this.vlines[g.x]);
                    Array.prototype.push.apply(ary, _this.hlines[g.y]);
                    if (ary.some(function (e) {
                        return (e.val === v);
                    })) {
                        g.cand.splice(i, 1);
                    }
                }

                // 値が1つなら確定させる
                if (g.cand.length === 1) {
                    g.val = g.cand[0];
                    g.cand = [];

                    // 確定したら再帰
                    _this.compensate();
                    return;
                } else {
                    _this.minLength = (g.cand.length > _this.minLength) ? _this.minLength : g.cand.length;
                }
            });

            // 候補数が少ないものを一つ確定させる
            if (this.minLength > 0) {
                var g = this.grids.filter(function (e) {
                    return (e.cand.length === _this.minLength);
                })[0];
                if (g) {
                    g.val = g.cand[Math.floor(Math.random() * this.minLength)];
                    g.cand = [];

                    // 確定したら再帰
                    this.compensate();
                    return;
                }
            } else if (this.grids.some(function (e) {
                return (e.val === 0);
            })) {
                this.init();
                this.compensate();
            }
        };
        Numpre.prototype.generate = function () {
            this.iter = 0;

            // 一発目
            this.init();
            this.compensate();
            this.grids.forEach(function (g) {
                g.val2 = g.val;
            });

            // 数字を1つずつ抜きながら破綻しないかを見る
            var r, res;
            var trueGrids;
            var cnt = 0;
            for (var i = 0; i < 100; i++) {
                trueGrids = this.grids.filter(function (e) {
                    return (e.visible === true);
                });
                r = Math.floor(Math.random() * trueGrids.length);
                trueGrids[r].visible = false;
                this.grids.filter(function (g) {
                    return (g.visible === false);
                }).forEach(function (g) {
                    g.val2 = 0;
                });
                res = this.solve();

                // 破綻したら元に戻す
                if (!res) {
                    trueGrids[r].visible = true;
                    this.grids.forEach(function (g) {
                        g.val2 = g.val;
                    });
                } else {
                    cnt++;
                    if (cnt > 30) {
                        break;
                    }
                }
            }

            // 隠すべきところの数字を隠す
            this.grids.filter(function (g) {
                return (g.visible === false);
            }).forEach(function (g) {
                g.val2 = 0;
            });

            /*
            console.log(this.iter);
            console.log(this.vlines[1].map(function(g){return g.val2}));
            console.log(this.vlines[2].map(function(g){return g.val2}));
            console.log(this.vlines[3].map(function(g){return g.val2}));
            console.log(this.vlines[4].map(function(g){return g.val2}));
            console.log(this.vlines[5].map(function(g){return g.val2}));
            console.log(this.vlines[6].map(function(g){return g.val2}));
            console.log(this.vlines[7].map(function(g){return g.val2}));
            console.log(this.vlines[8].map(function(g){return g.val2}));
            console.log(this.vlines[9].map(function(g){return g.val2}));
            console.log(this.grids.filter(function(g){return !g.visible}).length);
            */
            return this.grids;
        };
        return Numpre;
    })();
    TCG.Numpre = Numpre;
})(TCG || (TCG = {}));
//# sourceMappingURL=main.js.map
